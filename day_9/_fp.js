const data = require('./data');

var sum = (s,state,a,b,r) => s[modeValWrite(state,r)] = modeVal(s,state, a) + modeVal(s,state, b)
var mult = (s,state,a,b,r) => s[modeValWrite(state,r)] = modeVal(s,state, a) * modeVal(s,state, b)
var inputArrayFn = (input) => (s,state,r) => s[modeValWrite(state,r)] = input.shift()
var output = (s,state,a) => modeVal(s,state, a)
var jumpIfTrue = (s,state,a,b) => modeVal(s,state,a) !== 0 ? modeVal(s,state, b) : null
var jumpIfFalse = (s,state,a,b) => modeVal(s,state,a) === 0 ? modeVal(s,state, b) : null
var lessThan = (s,state,a,b,r) => s[modeValWrite(state,r)] = +(modeVal(s,state,a) < modeVal(s,state,b))
var equals = (s,state,a,b,r) => s[modeValWrite(state,r)] = +(modeVal(s,state,a) === modeVal(s,state,b))
var adjustRelBase = (s,state,a) => state.relBase += modeVal(s,state,a)

var modeVal = (s,state,v) => v[1] === 0 ? s[v[0]] : v[1] === 1 ? v[0] : s[v[0] + state.relBase]
var modeValWrite = (state,d) => d[1] === 0 ? d[0] : d[0] + state.relBase

var getOpCode = (v) => {
  const fullNumber = v.toString().padStart(5,'0')
  return `${fullNumber.charAt(3)}${fullNumber.charAt(4)}`
}

var getMode = (v, position) => {
  const fullNumber = v.toString().padStart(5,'0')
  return +fullNumber.charAt(position)
}

var configuration = (input) => {
  input = input === undefined ? [1] : input
  return {
    '01': {
      operation: sum,
      numberOfParameters: 3,
    },
    '02': {
      operation: mult,
      numberOfParameters: 3,
    },
    '03': {
      operation: inputArrayFn(input),
      numberOfParameters: 1,
    },
    '04': {
      operation: output,
      numberOfParameters: 1,
      type: 'output'
    },
    '05': {
      operation: jumpIfTrue,
      numberOfParameters: 2,
      type: 'jump'
    },
    '06': {
      operation: jumpIfFalse,
      numberOfParameters: 2,
      type: 'jump'
    },
    '07': {
      operation: lessThan,
      numberOfParameters: 3,
    },
    '08': {
      operation: equals,
      numberOfParameters: 3,
    },
    '09': {
      operation: adjustRelBase,
      numberOfParameters: 1,
    }
  }
}

var program = (stack, config, state) => {
  config = config || configuration()
  state = state || {i: 0, relBase: 0}
  for (let i = state.i; i < stack.length; i++) {
    // console.log('Read at', i);
    var v = stack[i]
    var opcode = getOpCode(v)
    var p = config[opcode]
    if (!p) {
      // console.log('halt')
      return 'halt'
    }
    var operation = p.operation;
    var parameters = [];
    for (let j = 0; j < p.numberOfParameters; j++) {
      var mode = getMode(v, 2 - j)
      parameters.push([stack[++i], mode])
    }
    var output = operation(stack, state, ...parameters)
    // console.log({index: i, value: v, opcode, operation, parameters, output, state, stack: [...stack]})
    if (output !== null) {
      if (p.type === 'output') {
        // console.log('Output', output)
        state.i = i + 1;
        if (p.returnValue) {
          return output
        }
      } else if (p.type === 'jump') {
        // console.log('Jump to ', output);
        i = output - 1;
      }
    }
  }
}

const example1 = [109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99]
const example2 = [1102,34915192,34915192,7,4,7,99,0]
const example3 = [104,1125899906842624,99]

// console.log(program(example3, configuration()))

const d9p1 = () => {
  return program(data, configuration([1]))
}

const d9p2 = () => {
  return program(data, configuration([2]))
}

// console.log(d9p2())

exports.intCodeComputer = program
exports.configuration = configuration