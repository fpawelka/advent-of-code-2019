const machine = require('../day_9/_fp')
const data = require('./data');

const paintArea = (input, startColor) => {
  const state = {
    stack: input,
    direction: 'U',
    i: 0, // pointer for program
    relBase: 0,
    currentPanel: [0,0],
    area: {}
  }

  let config
  let currentInput
  let currentOutput
  let outputStack

  while (currentOutput !== 'halt') {
    const [x, y] = state.currentPanel
    currentInput = state.area[`${x},${y}`] || startColor || 0

    if (startColor) {
      startColor = undefined
    }

    config = machine.configuration([currentInput])
    config['04'].returnValue = true

    outputStack = [
      machine.intCodeComputer(state.stack, config, state),
      machine.intCodeComputer(state.stack, config, state),
    ]

    if (outputStack[0] !== 'halt' && outputStack[1] !== 'halt') {
      paintPanel(outputStack[0], state)
      turnPainter(outputStack[1], state)
      movePainter(state)

    }

    currentOutput = outputStack[1]
  }

  return state.area
}

const paintPanel = (color, state) => {
  const [x, y] = state.currentPanel
  state.area[`${x},${y}`] = color
}

const turnPainter = (turnType, state) => {
  const direction = turnType === 0 ? -1 : 1
  const directions = ['U', 'R', 'D', 'L']
  let directionIndex = directions.indexOf(state.direction) + direction
  if (directionIndex < 0) {
    directionIndex = directions.length - 1
  } else if (directionIndex >= directions.length) {
    directionIndex = 0
  }
  state.direction = directions[directionIndex]
}

const move = {
  U: ([x, y], steps) => [x, y+steps],
  D: ([x, y], steps) => [x, y-steps],
  R: ([x, y], steps) => [x+steps, y],
  L: ([x, y], steps) => [x-steps, y],
}

const movePainter = state => {
  state.currentPanel = move[state.direction](state.currentPanel, 1)
}

// TODO
// const renderPainting

const renderArea = area => {

  const [xmin, ymin, xmax, ymax] = Object.keys(area).reduce((p, k) => {
    const [x, y] = k.split(',')
    return [
      Math.min(p[0], +x),
      Math.min(p[1], +y),
      Math.max(p[2], +x),
      Math.max(p[3], +y),
    ]
  }, [0, 0, 0, 0])

  const lines = []
  let line
  for (let y = ymax; y >= ymin; y--) {
    line = ''
    for (let x = xmin; x <= xmax; x++) {
      line += area[`${x},${y}`] === 0 ? ' ' : '#'
    }
    lines.push(line)
  }
  return lines.join("\n")
}


const d11p1 = (input) => {
  const area = paintArea(input, 0)
  return Object.keys(area).length
}


const d11p2 = (input) => {
  const area = paintArea(input, 1)
  return renderArea(area)
}

// console.log(d11p1(data))
console.log(d11p2(data))