const data = require('./data');


const getLayers = (input, width, height) => {
  const layers = [];
  let currentLayer = [];
  let line = [];
  for (let index = 0; index < input.length; index++) {
    line.push(+input.charAt(index))
    if (line.length >= width) {
      currentLayer.push([...line])
      if (currentLayer.length >= height) {
        layers.push([...currentLayer])
        currentLayer = []
      }
      line = [];
    }
  }
  return layers;
}

const getStackedLayer = (input, width, height) => {
  const layer = []
  const layerSize = width * height
  const numOfLayers = input.length / layerSize
  let line
  // rows
  for (let i = 0; i < layerSize; i += width) {
    line = []
    // columns
    for (let j = i; j < i + width; j++) {
      stack = []
      for (let k = j; k < numOfLayers * layerSize; k += layerSize) {
        stack.push(+input.charAt(k))
      }
      line.push([...stack])

    }
    layer.push([...line])
  }
  return layer;
}

const count = (str, layers) => layers
  .map(layer => layer
    .map(line => line.filter(v => v === str).length)
    .reduce((p, v) => p + v)
  )

const d8p1 = (input, width, height) => {
  const layers = getLayers(input, width, height)
  const countZeros = count(0, layers)
  const layer = layers[countZeros.indexOf(Math.min(...countZeros))]
  return count(1, [layer]) * count(2, [layer])
}

// console.log(d8p1(`123456789012`, 3, 2))
// console.log(d8p1(data, 25, 6))

const d8p2 = (input, width, height) => {
  const stackLayer = getStackedLayer(input, width, height)
  const reducedLayer = stackLayer.map(line => line.map(stack =>
    stack.reduce((p,v) => p === 2 ? v : p)
  ))
  const renderedLayer = reducedLayer.map(line => line.reduce((p, v) => p + (v === 0 ? '#' : ' '), ''))
  return renderedLayer
}
console.log(d8p2(data, 25, 6))
