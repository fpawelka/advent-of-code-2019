const onlyIncreasingNumbers = (num) => {
  strNum = num.toString();
  for (let index = 1; index < strNum.length; index++) {
    if (+strNum.charAt(index - 1) > +strNum.charAt(index)) {
      return false;
    }
  }
  return true;
}
const hasAjacentDigits = (num) => {
  strNum = num.toString();
  for (let index = 1; index < strNum.length; index++) {
    if (strNum.charAt(index - 1) === strNum.charAt(index)) {
      return true;
    }
  }
  return false;
};

const hasTwoAjacentDigits = (num) => {
  strNum = num.toString();
  currentGroupCounter = 1;
  hasGroupWithTwo = false;
  for (let index = 1; index < strNum.length; index++) {
    if (strNum.charAt(index - 1) === strNum.charAt(index)) {
      currentGroupCounter++;
    } else {
      if (currentGroupCounter === 2) {
        return true;
      }
      currentGroupCounter = 1;
    }
  }
  return currentGroupCounter === 2;
};

console.log('hasAjacentDigits(221111)', hasTwoAjacentDigits(221111));
console.log('hasAjacentDigits(222111)', hasTwoAjacentDigits(222111));
console.log('hasAjacentDigits(222211)', hasTwoAjacentDigits(222211));
console.log('hasAjacentDigits(221322)', hasTwoAjacentDigits(221322));

const filters1 = [
  onlyIncreasingNumbers,
  hasAjacentDigits,
];

const filters2 = [
  onlyIncreasingNumbers,
  hasTwoAjacentDigits,
]

const countPossibilities = (from, to, filters) => {
  let counter = 0;
  let valid;
  for (let i = from; i <= to; i++) {
    valid = true;
    for (let index = 0; index < filters.length; index++) {
      const filter = filters[index];
      if (!filter(i)) {
        valid = false;
        break;
      }
    }
    if (valid) {
      counter++;
    }
  }
  return counter;
}

console.log('### Day 4 - Part 1 ###');
console.log(countPossibilities(178416, 676461, filters1));

console.log('### Day 4 - Part 2 ###');
console.log(countPossibilities(178416, 676461, filters2));