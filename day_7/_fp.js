const data = require('./data');

var sum = (s,a,b,r) => s[r[0]] = modeVal(s, a) + modeVal(s, b)
var mult = (s,a,b,r) => s[r[0]] = modeVal(s, a) * modeVal(s, b)
var inputArrayFn = (input) => (s,r) => s[r[0]] = input.shift()
var output = (s,a) => modeVal(s, a)
var jumpIfTrue = (s,a,b) => modeVal(s,a) !== 0 ? modeVal(s, b) : null
var jumpIfFalse = (s,a,b) => modeVal(s,a) === 0 ? modeVal(s, b) : null
var lessThan = (s,a,b,r) => s[r[0]] = +(modeVal(s,a) < modeVal(s,b))
var equals = (s,a,b,r) => s[r[0]] = +(modeVal(s,a) === modeVal(s,b))

var modeVal = (s,v) => v[1] === 0 ? s[v[0]] : v[0]

var getOpCode = (v) => {
  const fullNumber = v.toString().padStart(5,'0')
  return `${fullNumber.charAt(3)}${fullNumber.charAt(4)}`
}

var getMode = (v, position) => {
  const fullNumber = v.toString().padStart(5,'0')
  return +fullNumber.charAt(position)
}

var configuration = (input) => {
  input = input === undefined ? 1 : input
  return {
    '01': {
      operation: sum,
      numberOfParameters: 3,
    },
    '02': {
      operation: mult,
      numberOfParameters: 3,
    },
    '03': {
      operation: inputArrayFn(input),
      numberOfParameters: 1,
      type: 'input'
    },
    '04': {
      operation: output,
      numberOfParameters: 1,
      type: 'output'
    },
    '05': {
      operation: jumpIfTrue,
      numberOfParameters: 2,
      type: 'jump'
    },
    '06': {
      operation: jumpIfFalse,
      numberOfParameters: 2,
      type: 'jump'
    },
    '07': {
      operation: lessThan,
      numberOfParameters: 3,
    },
    '08': {
      operation: equals,
      numberOfParameters: 3,
    },
  }
}

var program = (stack, config, state) => {
  state = state || {i: 0}
  for (let i = state.i; i < stack.length; i++) {
    // console.log('Read at', i);
    var v = stack[i]
    var opcode = getOpCode(v)
    var p = config[opcode]
    if (!p) {
      // console.log('halt')
      return 'halt'
    }
    var operation = p.operation;
    var parameters = [];
    for (let j = 0; j < p.numberOfParameters; j++) {
      var mode = getMode(v, 2 - j)
      parameters.push([stack[++i], mode])
    }
    var output = operation(stack, ...parameters)
    // console.log({index: i, value: v, opcode, operation, parameters, output, stack: [...stack]})
    if (output !== null) {
      if (p.type === 'output') {
        // console.log('Output', {output, i})
        state.i = i + 1;
        return output
      } else if (p.type === 'jump') {
        // console.log('Jump to ', output);
        i = output - 1;
      }
    }
  }
}

const runAmp = (stack, inputs, state) => program(stack, configuration(inputs), state)

const thrustersSignal = (stack, initialInput, phaseSettings) => {
  let ampOutput = initialInput
  phaseSettings.forEach(phaseSetting => ampOutput = runAmp([...stack], [phaseSetting, ampOutput]))
  return ampOutput
}

const possiblePhaseSettings = (numberOfAmplifiers, offset) => permutations(Array.from({length: numberOfAmplifiers}, (_, i) => i + (offset || 0)))

const permutations = (arr) => {
  if (arr.length === 1) {
    return [arr]
  }
  return arr
    .map(v => permutations(arr.filter(a => a !== v)).map(p =>[v, ...p]))
    .reduce((prev, curr) => [...prev, ...curr], [])
}

const d7p1 = (stack) => {
  const phaseSettings = possiblePhaseSettings(5);
  const initialInput = 0;
  return Math.max(...phaseSettings.map(phaseSetting => thrustersSignal(stack, initialInput, phaseSetting)))
}

const exampl1 = [3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0]
const exampl2 = [3,23,3,24,1002,24,10,24,1002,23,-1,23,101,5,23,23,1,24,23,23,4,23,99,0,0]
const exampl3 = [3,31,3,32,1002,32,10,32,1001,31,-2,31,1007,31,0,33,1002,33,7,33,1,33,31,31,1,32,31,31,4,31,99,0,0,0]

// console.log(d7p1(exampl1))
// console.log(d7p1(exampl2))
console.log(d7p1(data))

const thrustersLoopSignal = (stack, initialInput, phaseSettings) => {
  let newOutput = initialInput
  let currentAmpIndex = 0
  let amp, finalOutput, inputs;

  const amps = phaseSettings.map(v => ({stack: [...stack], phaseSetting: v, state: {i: 0}}))
  // console.log(amps)

  let i = 0;
  while (i++ < 100) {
    amp = amps[currentAmpIndex]
    inputs = [newOutput]
    if (amp.state.i === 0) {
      inputs.unshift(amp.phaseSetting)
    }
    newOutput = runAmp(amp.stack, inputs, amp.state)
    if (newOutput === 'halt') {
      finalOutput = amps[phaseSettings.length - 1].output
      return finalOutput
    }
    amp.output = newOutput
    currentAmpIndex++
    if (currentAmpIndex === phaseSettings.length) {
      currentAmpIndex = 0
    }
  }
}

const d7p2 = (stack) => {
  const phaseSettings = possiblePhaseSettings(5, 5)
  const initialInput = 0;
  return Math.max(...phaseSettings.map(phaseSetting => thrustersLoopSignal(stack, initialInput, phaseSetting)))
}

const example4 = [3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26,27,4,27,1001,28,-1,28,1005,28,6,99,0,0,5]
const example5 = [3,52,1001,52,-5,52,3,53,1,52,56,54,1007,54,5,55,1005,55,26,1001,54,-5,54,1105,1,12,1,53,54,53,1008,54,0,55,1001,55,1,55,2,53,55,53,4,53,1001,56,-1,56,1005,56,6,99,0,0,0,0,10]

// console.log(d7p2(example4))
console.log(d7p2(data))
