const data = require('./data');

const buildMap = (relations) => {
  const map = {}
  let planetA, planetB
  relations.forEach(relation => {
    [planetA, planetB] = relation.split(')')
    if (!map[planetA]) {
      map[planetA] = {
        name: planetA,
        children: [],
      }
    }
    if (!map[planetB]) {
      map[planetB] = {
        name: planetB,
        children: [],
      }
    }
    map[planetA].children.push(map[planetB])
    map[planetB].parent = map[planetA]
  });
  return map;
}

const countAllOrbits = (map) => Object.values(map).reduce((count, planet) => count += countOrbitsOfPlanet(planet), 0)
const countOrbitsOfPlanet = (planet) => planet.parent ? 1 + countOrbitsOfPlanet(planet.parent) : 0

const d6p1 = (input) => {
  const map = buildMap(input);
  const numberOfOrbits = countAllOrbits(map);
  console.log(numberOfOrbits);
}

const d6p2 = (input) => {
  const map = buildMap(input);
  const path = pathFromTo(map.YOU, map.SAN);
  console.log(path.length - 1);
}

const pathFromTo = (start, destination) => {
  const pathToRoot = (path, node) => path[node.name] = node && node.parent ? pathToRoot(path, node.parent) : path
  const getIntersection = (node, otherPath) => (!node || otherPath[node.name]) ? node : getIntersection(node.parent, otherPath)
  const pathTo = (path, node, to) => path.push(node.name) && (node.name !== to.name && node.parent) ? pathTo(path, node.parent, to) : path
  fromPath = pathToRoot({}, start)
  const intersection = getIntersection(destination, fromPath)
  const pathStartToIntersection = pathTo([], start, intersection)
  const pathDestinationToIntersection = pathTo([], destination, intersection)
  const path = [...pathStartToIntersection.slice(1), ...pathDestinationToIntersection.slice(1).slice(0,-1).reverse()]
  return path;
}

const example = `COM)B
B)C
C)D
D)E
E)F
B)G
G)H
D)I
E)J
J)K
K)L`.split("\n")

// d6p1(example);
console.log('### Day 6 Part 1');
d6p1(data);
console.log('### Day 6 Part 2');
d6p2(data);