

const example1 =
`......#.#.
#..#.#....
..#######.
.#.#.###..
.#..#.....
..#....#.#
#..#....#.
.##.#..###
##...#..#.
.#....####`

const example2 =
`.#..#
.....
#####
....#
...##`

const example3 =
`.#..##.###...#######
##.############..##.
.#.######.########.#
.###.#######.####.#.
#####.##.#.##.###.##
..#####..#.#########
####################
#.####....###.#.#.##
##.#################
#####.##.###..####..
..######..##.#######
####.##.####...##..#
.#####..#.######.###
##...#.##########...
#.##########.#######
.####.#.###.###.#.##
....##.##.###..#####
.#.#.###########.###
#.#.#.#####.####.###
###.##.####.##.#..##`

const data =
`##.##..#.####...#.#.####
##.###..##.#######..##..
..######.###.#.##.######
.#######.####.##.#.###.#
..#...##.#.....#####..##
#..###.#...#..###.#..#..
###..#.##.####.#..##..##
.##.##....###.#..#....#.
########..#####..#######
##..#..##.#..##.#.#.#..#
##.#.##.######.#####....
###.##...#.##...#.######
###...##.####..##..#####
##.#...#.#.....######.##
.#...####..####.##...##.
#.#########..###..#.####
#.##..###.#.######.#####
##..##.##...####.#...##.
###...###.##.####.#.##..
####.#.....###..#.####.#
##.####..##.#.##..##.#.#
#####..#...####..##..#.#
.##.##.##...###.##...###
..###.########.#.###..#.`

const buildMap = input => {
  const map = {}
  const lines = input.split("\n")
  lines.forEach((v, i) => v.split('').forEach((w, j) => w === '#' ? map[`${j},${i}`] = 0 : null))
  return map
}

const buildRelations = map => {
  const relations = {}
  Object.keys(map).forEach(a => Object.keys(map).forEach(b => a !== b ? relations[`${a}:${b}`] = null : null))
  return relations
}

const getBetween = (a, b, map) => {
  const [ax, ay] = a.split(',').map(v => +v)
  const [bx, by] = b.split(',').map(v => +v)

  const xDistance = bx - ax
  const yDistance = by - ay


  if (Math.abs(xDistance) === 1 || Math.abs(yDistance) === 1) {
    return;
  }

  let cx, cy, c

  if (xDistance === 0) {
    cx = ax
    for (let i = 1; i <= Math.abs(yDistance); i++) {
      cy = yDistance < 0 ? ay - i : ay + i
      c = `${cx},${cy}`
      if (map[c] !== undefined) {
        return c
      }
    }
  } else {
    const f = x => (yDistance / xDistance) * (x - ax) + ay
    for (let i = 1; i <= Math.abs(xDistance); i++) {
      cx = xDistance < 0 ? ax - i : ax + i
      cy = f(cx)
      // if (Math.abs(xDistance) > 1 || Math.abs(yDistance) > 1) console.log(cx, cy)
      if (!Number.isInteger(cy)) {
        continue;
      }
      c = `${cx},${cy}`
      if (map[c] !== undefined) {
        return c
      }
    }
  }
}

const countAsteorids = (map, relations) => {
  Object.keys(relations)
    .filter(relation => relations[relation])
    .forEach(relation => {
      const [a, b] = relation.split(':')
      map[a]++
      // map[b]++
    })
  return map
}

const keyWithMaxValue = obj => Object.keys(obj).reduce((p, v) => obj[p] > obj[v] ? p : v)

const renderMap = (points, width, height) => {
  let str = '';
  for (let i = 0; i < height; i++) {
    for (let j = 0; j < width; j++) {
      const address = `${j},${i}`
      if (points[address] !== undefined) {
        // str += 'O'
        str += points[address]
      } else {
        str += '.'
      }
    }
    str += "\n"
  }
  return str
}

const d10p1 = (input) => {
  const map = buildMap(input)
  const relations = buildRelations(map)
  Object.keys(relations)
    .forEach(relation => {
      if (relations[relation] !== null) {
        return
      }
      const [a, b] = relation.split(':')
      const c = getBetween(a, b, map)
      if (c && c !== b) {
        relations[`${a}:${b}`] = false
        relations[`${b}:${a}`] = false
        relations[`${a}:${c}`] = true
        relations[`${c}:${a}`] = true

      } else {
        relations[`${a}:${b}`] = true
        relations[`${b}:${a}`] = true
      }
    })
  const asteorids = countAsteorids({...map}, relations)
  const bestAsteorid = keyWithMaxValue(asteorids)

  const renderedMap = renderMap(asteorids, 5, 5)
  // console.log(renderedMap)

  return [bestAsteorid, asteorids[bestAsteorid]]
}

console.log(d10p1(data))

const Victor = require('victor')

const createVectorRelativTo = (address, base) => {
  const [x, y] = address.split(',')
  const [basex, basey] = base.split(',')
  return new Victor(+x - +basex, +basey - +y)
}

const d10p2 = (input, station, nth) => {
  const map = buildMap(input)
  delete map[station]

  const asteorids = Object.keys(map)
  .map(address => ({
    address: address,
    vector: createVectorRelativTo(address, station)
  }))
  .map(a => ({
    ...a,
    angle: a.vector.verticalAngle(),
    distance: a.vector.length()
  }))
  .map(a => ({
    ...a,
    angle: a.angle >= 0 ? a.angle : a.angle += 2 * Math.PI
  }))

  const groupedAsteorids = asteorids
    .sort((a, b) => a.angle - b.angle)
    .reduce((p, v) => {
      if (!p[v.angle]) p[v.angle] = []
      p[v.angle].push(v);
      return p
    }, {})

  const groupedAsteoridsArray = Object.keys(groupedAsteorids).map(a => ({
    angle: a,
    asteorids: groupedAsteorids[a]
      .sort((a, b) => a.distance - b.distance)
      .map(a => a.address)
    })
  )

  const asteoridsToDestroy = asteorids.length
  const destroyedAsteorids = []

  let i = 0
  while (destroyedAsteorids.length < asteoridsToDestroy) {
    if (groupedAsteoridsArray[i].asteorids.length) {
      destroyedAsteorids.push(groupedAsteoridsArray[i].asteorids.shift())
    }
    i++
    if (i >= groupedAsteoridsArray.length) {
      i = 0
    }
  }

  const asteorid = destroyedAsteorids[nth]
  if (!asteorid) return
  const [x,y] = asteorid.split(',')
  return x * 100 + +y
}

console.log(d10p2(data, '14,17', 199))
